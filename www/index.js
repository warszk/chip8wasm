import { Machine } from "chip8wasm";

const cpu = Machine.new();
console.log(cpu)
const renderLoop = () => {
  cpu.dump();
  requestAnimationFrame(renderLoop);
};

requestAnimationFrame(renderLoop);
