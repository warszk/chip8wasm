use crate::cpu::Cpu;
use crate::utils;
use crate::log;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct Machine {
    cpu: Cpu
}

#[wasm_bindgen]
impl Machine {
    pub fn new() -> Machine {
        utils::set_panic_hook();
        
        let cpu = Cpu::new();

        Machine { cpu }
    }

    pub fn load(&mut self) {
        self.cpu.load(vec!(0))
    }

    pub fn tick(&mut self) {
        self.cpu.emulate_cycle();
    }

    pub fn dump(&self) {
        log!("current opcode {:?} - v0: {:?}", self.cpu.opcode, self.cpu.registers);
    }
}
