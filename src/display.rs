const WIDTH: usize = 64;
const HEIGHT: usize = 32;

pub struct Display {
  pub memory: [u8; WIDTH*HEIGHT],
  dirty: bool
}

impl Display {
  pub fn new() -> Self {
    Display {
      memory: [0; WIDTH*HEIGHT],
      dirty: true
    }
  }

  pub fn cls(&mut self) {
    self.memory = [0; WIDTH*HEIGHT];
    self.dirty = false;
  }

  fn set_pixel(&mut self, x: usize, y: usize, val: u8) {
    self.memory[x + y * WIDTH] = val as u8;
  }

  fn get_pixel(&mut self, x: usize, y: usize) -> u8 {
    (self.memory[x + y * WIDTH] == 1) as u8
  }

  /// Sprites are XORed onto the existing screen. If this causes any pixels to be erased, VF is set to 1, otherwise it is set to 0.
  /// If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen.
  /// See instruction 8xy3 for more information on XOR, and section 2.4, Display, for more information on the Chip-8 screen and sprites.
  pub fn draw(&mut self, x: usize, y: usize, sprite: &[u8]) -> bool {
    self.dirty = false;
    for (j, &row) in sprite.iter().enumerate() {
      for i in 0..8 {
        let xi = (x + i) % WIDTH;
        let yi = (y + j) % HEIGHT;

        if row & (0x80 >> i) != 0x00 {
          let mut px = self.get_pixel(xi, yi);
          if px > 0 { self.dirty = true; }
          px ^= 0x01;
          self.set_pixel(xi, yi, px);
        }
      }
    }
    self.dirty
  }
}
