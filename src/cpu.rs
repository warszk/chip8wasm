use rand::random;
use crate::display::Display;
use crate::keypad::Keypad;
use crate::log;

/// build-in fonts
static FONTSET: [u8; 80] = [
  0xF0, 0x90, 0x90, 0x90, 0xF0, 0x20, 0x60, 0x20, 0x20, 0x70,
  0xF0, 0x10, 0xF0, 0x80, 0xF0, 0xF0, 0x10, 0xF0, 0x10, 0xF0,
  0x90, 0x90, 0xF0, 0x10, 0x10, 0xF0, 0x80, 0xF0, 0x10, 0xF0,
  0xF0, 0x80, 0xF0, 0x90, 0xF0, 0xF0, 0x10, 0x20, 0x40, 0x40,
  0xF0, 0x90, 0xF0, 0x90, 0xF0, 0xF0, 0x90, 0xF0, 0x10, 0xF0,
  0xF0, 0x90, 0xF0, 0x90, 0x90, 0xE0, 0x90, 0xE0, 0x90, 0xE0,
  0xF0, 0x80, 0x80, 0x80, 0xF0, 0xE0, 0x90, 0x90, 0x90, 0xE0,
  0xF0, 0x80, 0xF0, 0x80, 0xF0, 0xF0, 0x80, 0xF0, 0x80, 0x80
];

pub struct Cpu {
  pub opcode: u16,
  /// 16 general purpose 8-bit registers, referred to as V0, V1, ..., VF
  pub registers: [u8; 16],
  /// 4kb of memory
  pub memory: [u8; 4096],
  /// 16-bit program counter
  pub pc: u16,
  /// index 16-bit register, used for memory addresses
  pub i: u16,
  /// Two timer registers that count at 60 Hz. When set above zero they will count down to zero.
  /// sound timer
  pub sound_timer: u8,
  /// delay timer
  pub delay_timer: u8,
  /// display
  pub display: Display,
  /// keypad
  pub keypad: Keypad,
  /// stack of 16 16-bit values, used for subroutine calls
  pub stack: [u16; 16],
  /// 8-bit stack pointer
  pub sp: u8,
  /// waiting state for a keypress
  pub wait_for_key_state: (bool, u8)
}

impl Cpu {
  pub fn new() -> Self {
    let mut cpu = Cpu {
      opcode: 0,
      registers: [0; 16],
      memory: [0; 4096],
      pc: 0x200,
      i: 0,
      sound_timer: 0,
      delay_timer: 0,
      display: Display::new(),
      keypad: Keypad::new(),
      stack: [0; 16],
      sp: 0,
      wait_for_key_state: (false, 0x00)
    };

    // copy fontest into memory
    for (index, &byte) in FONTSET.iter().enumerate() {
      cpu.memory[index] = byte
    }

    cpu
  }

  /// load a program (an array of u8) into memory, starting from 0x200
  pub fn load(&mut self, program: Vec<u8>) {
    for (index, &byte) in program.iter().enumerate() {
      self.memory[0x200 + index] = byte;
    }
  }

  /// cpu cycle
  pub fn emulate_cycle(&mut self) {
    // fetch opcode
    self.opcode = (self.memory[self.pc as usize] as u16) << 8 | (self.memory[(self.pc + 1) as usize] as u16);
    // execute opcode
    self.execute_opcode();
    // update timers
    if self.delay_timer > 0 {
      self.delay_timer -= 1;
    }

    if self.sound_timer > 0 {
      if self.sound_timer == 1 { log!("BEEP") }
      self.sound_timer -= 1;
    }
  }

  /// process current opcode
  fn execute_opcode(&mut self) {
    let optuple = (
      ((self.opcode & 0xF000) >> 12) as u8,
      ((self.opcode & 0x0F00) >> 8 ) as u8,
      ((self.opcode & 0x00F0) >> 4 ) as u8,
      ((self.opcode & 0x000F)      ) as u8
    );

    let x = ((self.opcode & 0x0F00) >> 8) as usize;
    let y = ((self.opcode & 0x00F0) >> 4) as usize;
    let kk = (self.opcode & 0x00FF) as u8;
    let nnn = self.opcode & 0x0FFF;

    match optuple {
      // Clear the display.
      (0x0, 0x0, 0x0, 0x0) => {
        log!("CLS {} ", self.pc);
        self.display.cls();
        self.pc += 2;
      },
      // Return from a subroutine.
      // The interpreter sets the program counter to the address at the top of the stack, then subtracts 1 from the stack pointer.
      (0x0, 0x0, 0xE, 0xE) => {
        log!("RET");
        self.sp -= 1;
        self.pc = self.stack[self.sp as usize] + 2;
      },
      // Jump to location nnn.
      // The interpreter sets the program counter to nnn.
      (0x1, _, _, _) => {
        log!("JP addr");
        self.pc = nnn;
      },
      // Call subroutine at nnn.
      // The interpreter increments the stack pointer, then puts the current PC on the top of the stack. The PC is then set to nnn.
      (0x2, _, _, _) => {
        log!("CALL addr");
        self.stack[self.sp as usize] = self.pc;
        self.sp += 1;
        self.pc = nnn;
      },
      // Skip next instruction if Vx = kk.
      // The interpreter compares register Vx to kk, and if they are equal, increments the program counter by 2.
      (0x3, _, _, _) => {
        log!("SE Vx, byte");
        self.pc += if self.registers[x] == kk { 4 } else { 2 }
      },
      // Skip next instruction if Vx != kk.
      // The interpreter compares register Vx to kk, and if they are not equal, increments the program counter by 2.
      (0x4, _, _, _) => {
        log!("SNE Vx, byte");
        self.pc += if self.registers[x] != kk { 4 } else { 2 }
      },
      // Skip next instruction if Vx = Vy.
      // The interpreter compares register Vx to register Vy, and if they are equal, increments the program counter by 2.
      (0x5, _, _, 0x0) => {
        log!("SE Vx, Vy");
        self.pc += if self.registers[x] == self.registers[y] { 4 } else { 2 }
      },
      // Set Vx = kk.
      // The interpreter puts the value kk into register Vx.
      (0x6, _, _, _) => {
        log!("LD Vx, byte");
        self.registers[x] = kk;
        self.pc += 2;
      },
      // Set Vx = Vx + kk.
      // Adds the value kk to the value of register Vx, then stores the result in Vx.
      (0x7, _, _, _) => {
        log!("ADD Vx, byte");
        self.registers[x] += kk;
        self.pc += 2;
      },
      // Set Vx = Vy.
      // Stores the value of register Vy in register Vx.
      (0x8, _, _, 0x0) => {
        log!("LD Vx, Vy");
        self.registers[x] = self.registers[y];
        self.pc += 2;
      },
      // Set Vx = Vx OR Vy.
      // Performs a bitwise OR on the values of Vx and Vy, then stores the result in Vx. A bitwise OR compares the corrseponding bits from two values, and if either bit is 1, then the same bit in the result is also 1. Otherwise, it is 0.
      (0x8, _, _, 0x1) => {
        log!("OR Vx, Vy");
        self.registers[x] = self.registers[x] | self.registers[y];
        self.pc += 2;
      },
      // Set Vx = Vx AND Vy.
      // Performs a bitwise AND on the values of Vx and Vy, then stores the result in Vx. A bitwise AND compares the corrseponding bits from two values, and if both bits are 1, then the same bit in the result is also 1. Otherwise, it is 0.
      (0x8, _, _, 0x2) => {
        log!("AND Vx, Vy");
        self.registers[x] = self.registers[x] & self.registers[y];
        self.pc+= 2;
      },
      // Set Vx = Vx XOR Vy.
      // Performs a bitwise exclusive OR on the values of Vx and Vy, then stores the result in Vx. An exclusive OR compares the corrseponding bits from two values, and if the bits are not both the same, then the corresponding bit in the result is set to 1. Otherwise, it is 0.
      (0x8, _, _, 0x3) => {
        log!("XOR Vx, Vy");
        self.registers[x] = self.registers[x] ^ self.registers[y];
        self.pc += 2;
      },
      // Set Vx = Vx + Vy, set VF = carry.
      // The values of Vx and Vy are added together. If the result is greater than 8 bits (i.e., > 255,) VF is set to 1, otherwise 0. Only the lowest 8 bits of the result are kept, and stored in Vx.
      (0x8, _, _, 0x4) => {
        log!("ADD Vx, Vy");
        let res = self.registers[x] as u16 + self.registers[y] as u16;
        self.registers[0xF] = if res > 0xFF { 1 } else { 0 };
        self.registers[x] = (res & 0xFF) as u8;
        self.pc += 2;
      },
      // Set Vx = Vx - Vy, set VF = NOT borrow.
      // If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx.
      (0x8, _, _, 0x5) => {
        log!("SUB Vx, Vy");
        let res = self.registers[x] as i8 - self.registers[y] as i8;
        self.registers[0xF] = if res < 0 { 1 } else { 0 };
        self.registers[x] = res as u8;
        self.pc += 2;
      },
      // Set Vx = Vx SHR 1.
      // If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2.
      (0x8, _, _, 0x6) => {
        log!("SHR Vx, (, Vy)");
        self.registers[0xF] = self.registers[x] & 0x1;
        self.registers[x] >>= 1;
        self.pc += 2;
      },
      // Set Vx = Vy - Vx, set VF = NOT borrow.
      // If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.
      (0x8, _, _, 0x7) => {
        log!("SUBN Vx, Vy");
        let res = self.registers[y] as i8 - self.registers[x] as i8;
        self.registers[0xF] = if res < 0 { 1 } else { 0 };
        self.registers[x] = res as u8;
        self.pc += 2;
      },
      // Set Vx = Vx SHL 1.
      // If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2.
      (0x8, _, _, 0xE) => {
        log!("SHL Vx (, Vy)");
        self.registers[0xF] = self.registers[x] & 0x80;
        self.registers[x] <<= 1;
        self.pc += 2;
      },
      // Skip next instruction if Vx != Vy.
      // The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2.
      (0x9, _, _, 0x0) => {
        log!("SNE Vx, Vy");
        self.pc += if self.registers[x] != self.registers[y] { 4 } else { 2 };
      },
      // Set I = nnn.
      // The value of register I is set to nnn.
      (0xA, _, _, _) => {
        log!("LD I, addr");
        self.i = nnn;
        self.pc += 2;
      },
      // Jump to location nnn + V0.
      // The program counter is set to nnn plus the value of V0.
      (0xB, _, _, _) => {
        log!("JP V0, addr");
        self.pc = nnn + self.registers[0] as u16;
      },
      // Set Vx = random byte AND kk.
      // The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk. The results are stored in Vx. See instruction 8xy2 for more information on AND.
      (0xC, _, _, _) => {
        log!("RND Vx, byte");
        self.registers[x] = random::<u8>() & kk as u8;
        self.pc += 2;
      },
      // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
      // The interpreter reads n bytes from memory, starting at the address stored in I. These bytes are then displayed as sprites on screen at coordinates (Vx, Vy).
      (0xD, _, _, n) => {
        log!("DRW Vx, Vy, nibble");
        let collision = self.display.draw(self.registers[x] as usize, self.registers[y] as usize, &self.memory[self.i as usize .. (self.i + n as u16) as usize]);
        self.registers[0xF] = if collision { 1 } else { 0 };
        self.pc += 2;
      },
      // Skip next instruction if key with the value of Vx is pressed.
      // Checks the keyboard, and if the key corresponding to the value of Vx is currently in the down position, PC is increased by 2.
      (0xE, _, 0x9, 0xE) => {
        log!("SKP Vx");
        self.pc += if self.keypad.is_key_down(self.registers[x]) { 4 } else { 2 };
      },
      // Skip next instruction if key with the value of Vx is not pressed.
      // Checks the keyboard, and if the key corresponding to the value of Vx is currently in the up position, PC is increased by 2.
      (0xE, _, 0xA, 0x1) => {
        log!("SKPN Vx");
        self.pc += if self.keypad.is_key_down(self.registers[x]) { 2 } else { 4 };
      },
      // Set Vx = delay timer value.
      // The value of DT is placed into Vx.
      (0xF, _, 0x0, 0x7) => {
        log!("LD Vx, DT");
        self.registers[x] = self.delay_timer;
        self.pc += 2;
      },
      // Wait for a key press, store the value of the key in Vx.
      // All execution stops until a key is pressed, then the value of that key is stored in Vx.
      (0xF, _, 0x0, 0xA) => {
        log!("LD Vx, K");
        self.wait_for_key_state = (true, x as u8);
      },
      // Set delay timer = Vx.
      // DT is set equal to the value of Vx.
      (0xF, _, 0x1, 0x5) => {
        log!("LD DT, Vx");
        self.delay_timer = self.registers[x];
        self.pc += 2;
      },
      // Set sound timer = Vx.
      // ST is set equal to the value of Vx.
      (0xF, _, 0x1, 0x8) => {
        log!("LD ST, Vx");
        self.sound_timer = self.registers[x];
        self.pc += 2;
      },
      // Set I = I + Vx.
      // The values of I and Vx are added, and the results are stored in I.
      (0xF, _, 0x1, 0xE) => {
        log!("ADD I, Vx");
        self.i += self.registers[x] as u16;
        self.pc += 2;
      },
      // Set I = location of sprite for digit Vx.
      // The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx.
      (0xF, _, 0x2, 0x9) => {
        log!("LD F, Vx");
        self.i = self.registers[x] as u16 * 5;
        self.pc += 2;
      },
      // Store BCD representation of Vx in memory locations I, I+1, and I+2.
      // The interpreter takes the decimal value of Vx, and places the hundreds digit in memory at location in I, 
      // the tens digit at location I+1, and the ones digit at location I+2.
      (0xF, _, 0x3, 0x3) => {
        log!("LD B, Vx");
        self.memory[self.i as usize] = self.registers[x] / 100;
        self.memory[self.i as usize + 1] = (self.registers[x] % 100) / 10;
        self.memory[self.i as usize + 2] = self.registers[x] % 10;
        self.pc += 2;
      },
      // Store registers V0 through Vx in memory starting at location I.
      // The interpreter copies the values of registers V0 through Vx into memory, starting at the address in I.
      (0xF, _, 0x5, 0x5) => {
        log!("LD [I], Vx");
        self.memory[(self.i as usize) .. (self.i + x as u16 + 1) as usize].copy_from_slice(&self.registers[0..(x as usize + 1)]);
        self.pc += 2;
      },
      // Read registers V0 through Vx from memory starting at location I.
      // The interpreter reads values from memory starting at location I into registers V0 through Vx.
      (0xF, _, 0x6, 0x5) => {
        log!("LD Vx, [I]");
        self.registers[0 .. (x as usize + 1)].copy_from_slice(&self.memory[(self.i as usize) .. (self.i + x as u16 + 1) as usize]);
        self.pc += 2;
      },
      _ => {
        log!("Not implemented opcode {:0>4X} at {:0>5X}", self.opcode as usize, self.pc as usize);
      }
    }
  }
}
