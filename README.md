### Project setup

* install wasm, node 12, rust
* run npm install in www folder
* run wasm-pack build
* in www folder run npm run start

### Chip-8 docs
http://devernay.free.fr/hacks/chip8/C8TECH10.HTM

http://devernay.free.fr/hacks/chip8/

### Games
http://devernay.free.fr/hacks/chip8/GAMES.zip
